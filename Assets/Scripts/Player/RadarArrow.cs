using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadarArrow : MonoBehaviour
{

    List<GameObject> debrisParts = new List<GameObject>();
    [SerializeField]
    private Transform firePoint;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GameObject closestObject = null;
        if (gameObject.activeSelf)
        {
            closestObject = FindClosestDebrisPart();
        }
        if (transform.parent == null)
        {
            return;
        }

        Vector3 forwardDirection = firePoint.position - transform.parent.transform.position;
        Vector3 debrisDirection = closestObject.transform.position - transform.parent.transform.position;
        float angle = Vector3.SignedAngle(forwardDirection, debrisDirection, Vector3.forward);
        float currentRotationOfArrow = transform.localEulerAngles.z;
        if (currentRotationOfArrow >= 180)
        {
            currentRotationOfArrow = -180 + currentRotationOfArrow - 180;
        }
        float deltaRotation = angle - currentRotationOfArrow;
        transform.RotateAround(transform.parent.transform.position, Vector3.forward, deltaRotation);
    }

    private GameObject FindClosestDebrisPart()
    {
        if (transform.parent == null)
        {
            return null;
        }
        float minDistance = Mathf.Infinity;
        GameObject closestObject = null;
        foreach (GameObject part in debrisParts)
        {
            float distance = Vector2.Distance(transform.parent.transform.position, part.transform.position);
            if (distance < minDistance)
            {
                minDistance = distance;
                closestObject = part;
            }
        }
        return closestObject;
    }

    public void AddDebrisPart(GameObject debrisPart)
    {
        debrisParts.Add(debrisPart);
    }

    public void RemoveDebrisPart(GameObject debrisPart)
    {
        debrisParts.Remove(debrisPart);
    }
}
