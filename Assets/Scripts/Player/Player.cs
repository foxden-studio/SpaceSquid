using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private InventoryHandler inventory;
    [SerializeField]
    private Transform firePoint;
    [SerializeField]
    private HighscoreTimer highscoreTimer;

    private GameObject shieldLeftEquipped;
    private GameObject shieldRightEquipped;
    private GameObject plasmaShield;
    private GameObject radarArrow;

    [SerializeField]
    private float attackCooldown = 1f;
    [SerializeField]
    private float projectileSpeed = 7f;
    private float attackTimer = 0f;
    [SerializeField]
    private int maxWeaponUses = 3;
    private int weaponUses;
    [SerializeField]
    private float radarTime = 20f;
    private float radarTimer;
    [SerializeField]
    private float driveTime = 20f;
    private float driveTimer;

    // Start is called before the first frame update
    void Start()
    {
        shieldLeftEquipped = GameObject.Find("ShieldLeft");
        shieldLeftEquipped.SetActive(false);
        shieldRightEquipped = GameObject.Find("ShieldRight");
        shieldRightEquipped.SetActive(false);
        plasmaShield = GameObject.Find("PlasmaShield");
        plasmaShield.SetActive(false);
        radarArrow = GameObject.Find("RadarArrow");
        radarArrow.SetActive(false);
    }

    void Update()
    {
        attackTimer -= Time.deltaTime;
        radarTimer -= Time.deltaTime;
        driveTimer -= Time.deltaTime;
        if (this.inventory.hasWeapon() && Input.GetButton("Fire1") && attackTimer <= 0f)
        {
            attackTimer = attackCooldown;
            Attack();
        }
        if (radarTimer <= 0)
        {
            DisableRadar();
        }
        if (driveTimer <= 0)
        {
            this.inventory.setDrive(false);
        }
    }

    public void TakeDamage()
    {
        if (this.inventory.hasCore())
        {
            this.inventory.setCore(false);
            plasmaShield.SetActive(false);
            return;
        }
        transform.DetachChildren();
        Destroy(gameObject);
        highscoreTimer.EndGame();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void HitShield(string shield)
    {
        if (shield == "ShieldLeft")
        {
            this.inventory.setLeftShield(false);
            shieldLeftEquipped.SetActive(false);
        }
        if (shield == "ShieldRight")
        {
            this.inventory.setRightShield(false);
            shieldRightEquipped.SetActive(false);
        }
    }

    private void Attack()
    {
        if (weaponUses <= 0)
        {
            this.inventory.setWeapon(false);
            return;
        }

        Vector3 direction = new Vector3(
            transform.position.x - firePoint.position.x,
            transform.position.y - firePoint.position.y,
            transform.position.z - firePoint.position.z);

        Vector3 directionVector = direction.normalized;
        float rotation_z = Mathf.Atan2(directionVector.y, directionVector.x) * Mathf.Rad2Deg;

        GameObject projectilePrefab = FindObjectOfType<GameManager>().GetPrefab("Projectile");
        GameObject projectile = Instantiate(projectilePrefab, firePoint.position, Quaternion.Euler(0f, 0f, rotation_z));
        projectile.GetComponent<Projectile>().StartProjectile(gameObject, -directionVector, projectileSpeed);

        weaponUses--;
    }

    public bool CollectDebris(string part)
    {
        if (part == "Core")
        {
            if (!this.inventory.hasCore())
            {
                this.inventory.setCore(true);
                plasmaShield.SetActive(true);
                return true;
            }
        }
        if (part == "Shield")
        {
            if (!this.inventory.hasLeftShield())
            {
                this.inventory.setLeftShield(true);
                shieldLeftEquipped.SetActive(true);
                return true;
            }
            if (!this.inventory.hasRightShield())
            {
                this.inventory.setRightShield(true);
                shieldRightEquipped.SetActive(true);
                return true;
            }
        }
        if (part == "Weapon")
        {
            if (!this.inventory.hasWeapon())
            {
                this.inventory.setWeapon(true);
                weaponUses = maxWeaponUses;
                return true;
            }
            else
            {
                if (weaponUses != maxWeaponUses)
                {
                    weaponUses = maxWeaponUses;
                    return true;
                }
            }
        }
        if (part == "SpeedModule")
        {
            if (!this.inventory.hasSpeedModule())
            {
                this.inventory.setSpeedModule(true);
                radarArrow.SetActive(true);
                radarTimer = radarTime;
                return true;
            }
        }
        if (part == "Drive")
        {
            if (!this.inventory.hasDrive())
            {
                this.inventory.setDrive(true);
                driveTimer = driveTime;
                return true;
            }
        }
        return false;
    }

    public InventoryHandler getInventory()
    {
        return this.inventory;
    }

    private void DisableRadar()
    {
        this.inventory.setSpeedModule(false);
        radarArrow.SetActive(false);
    }
}
