using NoiseTest;
using UnityEngine;

public class FogGeneration : MonoBehaviour
{
    public int width;
    public int height;

    public float scale;

    public float offsetX;
    public float offsetY;

    public float fogSpeed;

    private float lastPlayerPosX = 0f;
    private float lastPlayerPosY = 0f;

    [SerializeField]
    private SpriteMask mask;
    private OpenSimplexNoise simplex;
    [SerializeField]
    private Transform player;

    void Start()
    {
        simplex = new OpenSimplexNoise();
        GenerateTexture();
    }

    void Update()
    {
        if (player == null)
        {
            return;
        }

        if (Mathf.Abs(player.transform.position.x - lastPlayerPosX) >= 5f ||
            Mathf.Abs(player.transform.position.y - lastPlayerPosY) >= 5f)
        {
            lastPlayerPosX = player.transform.position.x;
            lastPlayerPosY = player.transform.position.y;

            transform.position = player.transform.position;
            mask.transform.position = player.transform.position;

            GenerateTexture();
        }
    }


    Texture2D GenerateTexture()
    {
        Texture2D texture = new Texture2D(width, height);

        // Generate Perlin Noise Map for the texture

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Color color = CalculateColor(x, y);
                texture.SetPixel(x, y, color);
            }
        }
        texture.Apply();

        Rect rect = new Rect(0, 0, width, height);
        Sprite sprite = Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f));
        GetComponent<SpriteRenderer>().sprite = sprite;
        mask.GetComponent<SpriteMask>().sprite = sprite;
        return texture;
    }

    Color CalculateColor(int x, int y)
    {
        float xCoord1 = (float)x / (width - 1) * scale + offsetX + 
            player.transform.position.x * scale / (width * transform.localScale.x / 100);   // this part accounts for the players movement along the map
        float yCoord1 = (float)y / (height - 1) * scale + offsetY + 
            player.transform.position.y * scale / (height * transform.localScale.y / 100);

        float sample = (float)simplex.Evaluate(xCoord1, yCoord1);

        float sampleY;

        if (sample < 0f)
        {
            return new Color(0, 0, 0, 0);
        }
        else
        {
            sampleY = sample;
            return new Color(sampleY, sampleY, sampleY, 0.4f);
        }
    }
}
