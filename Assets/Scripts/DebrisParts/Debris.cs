using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debris : MonoBehaviour
{
    Player player;
    RadarArrow radarArrow;
    HighscoreTimer highscore;

    // Start is called before the first frame update
    void Start()
    {
        highscore = GameObject.Find("TimerPanel").GetComponent<HighscoreTimer>();
        if (GameObject.FindGameObjectsWithTag("Player").Length == 0)
        {
            return;
        }
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        radarArrow = player.gameObject.transform.Find("RadarArrow").GetComponent<RadarArrow>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider collision)
    {
        if (collision.tag == "Player")
        {
            if (player.CollectDebris(gameObject.tag))
            {
                FindObjectOfType<GameManager>().PlaySound("collect", GameManager.MixerGroup.SFX);
                radarArrow.RemoveDebrisPart(gameObject);
                highscore.CollectDebrisPart();
                Destroy(gameObject);
            }
        }
    }
}
