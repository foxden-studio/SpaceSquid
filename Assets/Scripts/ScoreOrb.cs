using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreOrb : MonoBehaviour
{

    private HighscoreTimer highscore;
    private Vector3 direction;

    // Start is called before the first frame update
    void Start()
    {
        highscore = GameObject.Find("TimerPanel").GetComponent<HighscoreTimer>();
        direction = new Vector3(Random.Range(-0.01f, 0.01f), Random.Range(-0.01f, 0.01f), 0);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position += direction;
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Player")
        {
            FindObjectOfType<GameManager>().PlaySound("orbcollect", GameManager.MixerGroup.SFX);
            highscore.CollectScoreOrb();
            Destroy(gameObject);
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
