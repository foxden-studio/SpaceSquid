using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    private Transform playerPosition;
    private Player player;
    [SerializeField]
    private Transform firePoint;
    [SerializeField]
    private FieldOfView fov;

    [HideInInspector]
    public bool canAttack = false;
    [SerializeField]
    private float attackRate = 1f;
    [SerializeField]
    private float chargeTime1 = 0.45f;
    [SerializeField]
    private float chargeTime2 = 0.05f;
    

    private float attackTimer = 0f;
    [SerializeField]
    private float projectileSpeed;
    private float currentProjectileSpeed;
    private float maxProjectileSpeedMultiplier = 1.3f;

    private Vector3 directionVector;
    private float rotation_z;


    // Start is called before the first frame update
    void Start()
    {
        if (GameObject.FindGameObjectsWithTag("Player").Length == 0)
        {
            return;
        }
        playerPosition = GameObject.FindGameObjectWithTag("Player").transform;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        currentProjectileSpeed = projectileSpeed * Mathf.Min(maxProjectileSpeedMultiplier, (1 + HighscoreTimer.gameTimer * 0.005f));
        attackRate = Mathf.Max(0.7f, 1 - 0.0025f * HighscoreTimer.gameTimer);
        if (playerPosition == null)
        {
            return;
        }

        attackTimer -= Time.deltaTime;
        if (canAttack && attackTimer <= 0f)
        {
            Charge();
        }
    }

    void Charge()
    {
        attackTimer = attackRate;
        StartCoroutine(Attack());
    }

    IEnumerator Attack()
    {
        GameObject projectilePrefab = FindObjectOfType<GameManager>().GetPrefab("Projectile");

        FindObjectOfType<GameManager>().PlaySound("charge", GameManager.MixerGroup.SFX);
        yield return new WaitForSeconds(chargeTime1);
        
        if (playerPosition == null)
        {
            yield break;
        }
        Vector3 playerPos = playerPosition.position;
        yield return new WaitForSeconds(chargeTime2);

        if (!canAttack)
        {
            yield break;
        }

        Vector3 direction = new Vector3(
            playerPos.x - firePoint.position.x,
            playerPos.y - firePoint.position.y,
            playerPos.z - firePoint.position.z);

        directionVector = direction.normalized;
        rotation_z = Mathf.Atan2(directionVector.y, directionVector.x) * Mathf.Rad2Deg;

        GameObject projectile = Instantiate(projectilePrefab, firePoint.position, Quaternion.Euler(0f, 0f, rotation_z));
        projectile.GetComponent<Projectile>().StartProjectile(gameObject, directionVector, currentProjectileSpeed);
    }

}
