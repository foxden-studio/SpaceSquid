using System.Collections;
using System.Collections.Generic;
using System.Runtime.ExceptionServices;
using UnityEngine;

public class ControlsOverlay : MonoBehaviour
{

    public static bool firstTimeLoaded = true;


    // Start is called before the first frame update
    void Start()
    {
        if (!firstTimeLoaded)
        {
            gameObject.SetActive(false);
        }
        else
        {
            Time.timeScale = 0f;
        }
        firstTimeLoaded = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKey)
        {
            Time.timeScale = 1f;
            gameObject.SetActive(false);
        }
    }
}
