using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HighscoreTimer : MonoBehaviour
{
    private Player playerObject;
    public static float gameTimer = 0f;
    private float scoreTimer = 0f;

    private int currentScore = 0;
    [SerializeField]
    private int scorePerOrb;
    [SerializeField]
    private int scorePerDebrisPart;
    [SerializeField]
    private Text timerText;
    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private AnimationCurve scoreCurve;

    // Start is called before the first frame update
    void Start()
    {
        this.playerObject = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        gameTimer += Time.deltaTime;
        scoreTimer += Time.deltaTime;
        if (scoreTimer >= 0.2f)
        {
            scoreTimer = 0f;
            AddScore();
        }

        UpdateTimer();
    }

    public void EndGame()
    {
        if (currentScore >= 100000)
        {
            AddHighscoreEntry(currentScore, "AAA");
            FindObjectOfType<GameManager>().PlaySound("highscore", GameManager.MixerGroup.SFX);
        }
        SceneManager.LoadScene(2);
    }

    private void UpdateTimer()
    {
        timerText.text = FormatTime(gameTimer);
        scoreText.text = currentScore.ToString();
    }

    private void AddScore()
    {
        currentScore += Mathf.Max(10, (int)scoreCurve.Evaluate(gameTimer));
    }

    public void CollectScoreOrb()
    {
        currentScore += scorePerOrb;
    }

    public void CollectDebrisPart()
    {
        currentScore += scorePerDebrisPart;
    }

    public void AddHighscoreEntry(int score, string name)
    {
        HighscoreEntry highscoreEntry = new HighscoreEntry { score = score, name = name };

        //Load saved Highscores
        string jsonString = PlayerPrefs.GetString("highscoreTable");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        if (highscores == null)
        {
            highscores = new Highscores();
            highscores.highscoreEntryList = new List<HighscoreEntry>();
        }

        //Add new entry to Highscores
        highscores.highscoreEntryList.Add(highscoreEntry);

        //Save updates Highscores
        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("highscoreTable", json);
        PlayerPrefs.Save();
    }
    private string FormatTime(float time)
    {
        int minutes = (int)time / 60;
        int seconds = (int)time - 60 * minutes;
        int milliseconds = (int)(1000 * (time - minutes * 60 - seconds));
        return string.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, milliseconds);
    }

    void OnEnable()
    {
        gameTimer = 0;
    }
}
