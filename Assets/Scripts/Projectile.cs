using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private GameObject initiator;
    private SpriteRenderer spriteRenderer;
    private Rigidbody rb;
    private bool hasHit = false;

    Player player;


    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
        if (playerObject == null)
        {
            return;
        }
        player = playerObject.GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartProjectile(GameObject initiator, Vector3 direction, float projectileSpeed)
    {
        this.initiator = initiator;
        rb.velocity = direction * projectileSpeed;

        if (Random.Range(0f, 1f) > 0.98f)
        {
            spriteRenderer.sprite = FindObjectOfType<GameManager>().GetSprite("TX Village Props_1");
            FindObjectOfType<GameManager>().PlaySound("arrow", GameManager.MixerGroup.SFX);
            return;
        }

        if (initiator.tag == "Enemy")
        {
            float random = Random.Range(0f, 1f);
            if (random < 0.3f)
            {
                FindObjectOfType<GameManager>().PlaySound("blaster2", GameManager.MixerGroup.SFX);
                return;
            }
            if (random < 0.7f)
            {
                FindObjectOfType<GameManager>().PlaySound("blaster3", GameManager.MixerGroup.SFX);
                return;
            }
            if (random >= 0.7f)
            {
                FindObjectOfType<GameManager>().PlaySound("blaster4", GameManager.MixerGroup.SFX);
                return;
            }
        }
        if (initiator.tag == "Player")
        {
            FindObjectOfType<GameManager>().PlaySound("blaster1", GameManager.MixerGroup.SFX);
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (initiator == null)
        {
            Destroy(gameObject);
            return;
        }

        if (initiator.tag == "Enemy")
        {
            if (collision.tag == "ShieldEquipped" && !hasHit)
            {
                hasHit = true;
                FindObjectOfType<GameManager>().PlaySound("shieldhit", GameManager.MixerGroup.SFX);
                this.triggerExplosion(collision.gameObject);
                player.HitShield(collision.gameObject.name);
                Destroy(gameObject);
            }

            if (collision.tag == "Player" && !hasHit)
            {
                hasHit = true;
                FindObjectOfType<GameManager>().PlaySound("playerhit", GameManager.MixerGroup.SFX);
                this.triggerExplosion(collision.gameObject);
                player.TakeDamage();
                Destroy(gameObject);
            }

            if (collision.tag == "Enemy" && !hasHit && collision.gameObject != initiator)
            {
                hasHit = true;
                FindObjectOfType<GameManager>().PlaySound("playerhit", GameManager.MixerGroup.SFX);
                this.triggerExplosion(collision.gameObject);
                GameObject scoreOrbPrefab = FindObjectOfType<GameManager>().GetPrefab("ScoreOrb");
                for (int i = 0; i < 3; i++)
                {
                    Instantiate(scoreOrbPrefab, collision.transform.position, Quaternion.identity);
                }
                Destroy(collision.gameObject);
                Destroy(gameObject);
            }
        }
        if (initiator.tag == "Player")
        {
            if (collision.tag == "Enemy")
            {
                FindObjectOfType<GameManager>().PlaySound("playerhit", GameManager.MixerGroup.SFX);
                this.triggerExplosion(collision.gameObject);
                GameObject scoreOrbPrefab = FindObjectOfType<GameManager>().GetPrefab("ScoreOrb");
                for (int i = 0; i < 3; i++)
                {
                    Instantiate(scoreOrbPrefab, collision.transform.position, Quaternion.identity);
                }
                Destroy(collision.gameObject);
                Destroy(gameObject);
            }
        }
    }

    private void triggerExplosion(GameObject gObject)
    {
        GameObject explosionPrefab = FindObjectOfType<GameManager>().GetPrefab("Explosion");
        Vector3 position = new Vector3(gObject.transform.position.x, gObject.transform.position.y, 0);
                
        Instantiate(explosionPrefab, position, Quaternion.identity);
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
